exports = function(newComment){
  const challengeId = newComment["thread_id"];

  // get challenge creator
  let challenges = context.services.get("mongodb-atlas").db("challe").collection("challenges");

  const challengesQuery = {
    "is_deleted": false,
    "_id": challengeId
  };
  console.log(JSON.stringify(challengesQuery));

  return challenges.findOne(challengesQuery)
    .then(challengesResult => {
      // do nothing if self
      const challengeCreatorId = challengesResult.creator_id;

      if(challengeCreatorId == newComment["creator_id"]) {

        return;

      } else {

        // get challenge creator's notification settings
        let uns = context.services.get("mongodb-atlas").db("challe").collection("user_notification_settings");

        const unsQuery = {
          "is_deleted": false,
          "creator_id": challengeCreatorId
        };
        console.log(JSON.stringify(unsQuery));

        return uns.findOne(unsQuery)
          .then(unsResult => {

            if(unsResult["receive_new_challenge_comment_notifs"] && unsResult["fcm_token"]) {

              return context.services.get("gcm").send({
                "registrationTokens": [unsResult["fcm_token"]],
                "priority": "normal",
                "notification": {
                    "body": newComment["creator"] + " commented on your challenge!",
                    "sound": "ping.aiff",
                },
                "data": {
                  "initial_view_identifier": "challengeViewController",
                  "challenge_id": String(challengeId) // sending as string easier for client json interpretation
                }
             });

           } else {

             return;

           }

          })
          .catch(err => console.error(`Failed to find uns: ${err}`));

      }

    })
    .catch(err => console.error(`Failed to find challenge: ${err}`));
};

// For testing:
// exports({
//   "creator_id": "5cbcca35aa8d5674cb8eda84",
//   "creator_image_url": "https://s.gravatar.com/avatar/4d590a1d27522d389bea1c894c6ce5a2?s=480&r=pg&d=https%3A%2F%2Fcdn.auth0.com%2Favatars%2Fld.png",
//   "thread_type": "challenge",
//   "thread_id": BSON.ObjectId("5cc84de6ad851c64ae30fdf2"),
//   "created_at": new Date(),
//   "is_deleted": false,
//   "n_flags": 0,
//   "creator": "ldoromol",
//   "text": "https://www.nytimes.com/interactive/2019/04/30/dining/climate-change-food-eating-habits.html?smid=nytcore-ios-share"
// })
