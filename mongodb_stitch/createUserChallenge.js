exports = function(newUserChallenge){
  console.log(JSON.stringify(newUserChallenge));

  let userChallenges = context.services.get("mongodb-atlas").db("challe").collection("user_challenges");
  let challengeId = newUserChallenge.challenge_id;
  let creatorId = newUserChallenge.creator_id;

  return context.functions.execute("getUserNotificationSettings", creatorId)
    .then(uns => {

      newUserChallenge['receive_challenge_reminders'] = uns["receive_challenge_reminders_default"];
      newUserChallenge['receive_challenge_reminders_at_utc_hour'] = uns["receive_challenge_reminders_at_utc_hour_default"];

      return userChallenges.insertOne(newUserChallenge)
      .then(result => {
        context.functions.execute("updateNFollowers", "challenges", challengeId, 1);
        context.functions.execute("sendNewChallengeJoinerNotification", newUserChallenge);
        console.log(JSON.stringify(result));
        return result.insertedId.toString();
        })
      .catch(err => {
        const str = `Failed to insert userChallenge: ${err}`;
        console.error(str);
        return str;
      });

    })
    .catch(err => {
      const str = `Failed to get userNotificationSettings: ${err}`;
      console.error(str);
      return str;
    });
};
