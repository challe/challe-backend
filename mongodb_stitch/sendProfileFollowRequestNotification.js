exports = function(newFollow){
  const followerId = newFollow["creator_id"];
  const followingId = newFollow["following_id"];

  // get follower username
  let users = context.services.get("mongodb-atlas").db("challe").collection("users");

  const usersQuery = {
    "is_deleted": false,
    "_id": BSON.ObjectId(followerId)
  };
  console.log(JSON.stringify(usersQuery));

  return users.findOne(usersQuery)
    .then(usersResult => {
      // get challenge creator's notification settings
      let uns = context.services.get("mongodb-atlas").db("challe").collection("user_notification_settings");

      const unsQuery = {
        "is_deleted": false,
        "creator_id": followingId
      };
      console.log(JSON.stringify(unsQuery));

      return uns.findOne(unsQuery)
        .then(unsResult => {

          if(unsResult["receive_profile_follow_request_notifs"] && unsResult["fcm_token"]) {

            return context.services.get("gcm").send({
              "registrationTokens": [unsResult["fcm_token"]],
              "priority": "normal",
              "notification": {
                  "body": usersResult["username"] + " requested to follow you!",
                  "sound": "ping.aiff",
              },
              "data": {
                "initial_view_identifier": "ownProfileViewController"
              }
           });

         } else {

           return;

         }

        })
        .catch(err => console.error(`Failed to find uns: ${err}`));

    })
    .catch(err => console.error(`Failed to find user: ${err}`));
};

// For testing:
// exports({
//   "creator_id":"5caa9e93b983934c9e7231ab",
//   "following_id": "5caa96ba082bf6ab87520766"
// })
