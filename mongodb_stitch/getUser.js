exports = function(userIdStr, withholdPII=true){
  const thisUser = context.user.id;
  console.log(thisUser);

  return context.functions.execute("getFollowedUserIds", thisUser, true, true, true)
    .then(followedUserIds => {
      console.log(JSON.stringify(followedUserIds));

      let users = context.services.get("mongodb-atlas").db("challe").collection("users");

      const query = {
        "is_deleted": false,
        "_id": BSON.ObjectId(userIdStr)
      };

      var pipeline = [
        {"$match": query}
      ];

      if(withholdPII) {
        pipeline.push(
          {"$project": {
            "phone_number": 0,
            "email": 0}}
        );
      }

      console.log(JSON.stringify(pipeline));

      return users.aggregate(pipeline)
        .toArray()
        .then(resultArr => {
          console.log(JSON.stringify(resultArr));

          const result = resultArr[0];
          result.user_is_follower = false; // initialize
          result.follow_request_pending = false; // initialize

          const userId = String(result._id); // indexOf doesn't work with mongo object arrays

          const fULength = followedUserIds.length;
          for (var i = 0; i < fULength; i++) {
            const followedUserId = followedUserIds[i]["following_id"];
            if(userId == followedUserId) {
              result.follow_request_pending = !followedUserIds[i]["is_approved"];
              result.user_is_follower = !result.follow_request_pending;
              break;
            }
          }

          console.log(JSON.stringify(result));
          return result;
        })
        .catch(err => console.error(`Failed to find user: ${err}`));
    })
    .catch(err => console.error(`Failed to find followed user ids: ${err}`));
};
