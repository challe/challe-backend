exports = function(challengeId, creatorId){

  let userChallenges = context.services.get("mongodb-atlas").db("challe").collection("user_challenges");

  const query = {
    "is_deleted": false,
    "challenge_id": challengeId,
    "creator_id": creatorId
  };
  console.log(JSON.stringify(query));

  const now = new Date();
  const update = {
    "$set": {
        "is_deleted": true,
        "deleted_at": now
    }
  };
  console.log(JSON.stringify(update));

  const options = { upsert: false };
  console.log(JSON.stringify(options));

  return userChallenges.updateOne(query, update, options)
  .then(result => {
    console.log(JSON.stringify(result));
    const { matchedCount, modifiedCount } = result;
    if(matchedCount && modifiedCount) {
      context.functions.execute("updateNFollowers", "challenges", challengeId, -1);
      return `Successfully deleted user challenge.`;
    }
  })
  .catch(err => console.error(`Failed to delete user challenge: ${err}`));
};
