exports = function(collectionName, targetId, byAmt=1){

  let itemsCollection = context.services.get("mongodb-atlas").db("challe").collection(collectionName);

  const query = {
    "_id": targetId
  };
  console.log(JSON.stringify(query));

  const update = {
    "$inc": { "n_followers": parseInt(byAmt, 10)}
  };
  console.log(JSON.stringify(update));

  const options = { upsert: true };
  console.log(JSON.stringify(options));

  itemsCollection.updateOne(query, update, options)
  .then(result => {
    console.log(JSON.stringify(result));

    const { matchedCount, modifiedCount } = result;
    if(matchedCount && modifiedCount) {
      console.log(`Successfully change followers.`);
    }
  })
  .catch(err => console.error(`Failed to change followers: ${err}`));

};
