exports = function(challengeId, creatorId){

  return context.functions.execute("getUserNotificationSettings", creatorId)
    .then(uns => {

      return context.functions.execute("getUserChallenge", challengeId, creatorId)
        .then(uc => {
          uc = uc[0];

          uc['timezone'] = uns['timezone'];

          console.log(JSON.stringify(uc));
          console.log(JSON.stringify(uc['receive_challenge_reminders']));
          console.log(JSON.stringify(uc['receive_challenge_reminders_at_utc_hour']));
          console.log(JSON.stringify(uc['timezone']));
          return uc;

        })
        .catch(err => {
          const str = `Failed to get userChallenge: ${err}`;
          console.error(str);
          return str;
        });
    })
    .catch(err => {
      const str = `Failed to get userNotificationSettings: ${err}`;
      console.error(str);
      return str;
    });

};
