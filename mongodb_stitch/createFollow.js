exports = function(newFollow){
  console.log(JSON.stringify(newFollow));

  let follows = context.services.get("mongodb-atlas").db("challe").collection("follows");
  let creatorId = BSON.ObjectId(newFollow.creator_id);
  let followingId = BSON.ObjectId(newFollow.following_id);

  return follows.insertOne(newFollow)
  .then(result => {
    if(newFollow.is_approved) {
      context.functions.execute("updateNFollowing", "users", creatorId, 1);
      context.functions.execute("updateNFollowers", "users", followingId, 1);
      context.functions.execute("sendNewProfileFollowerNotification", newFollow);
    } else {
      context.functions.execute("sendProfileFollowRequestNotification", newFollow);
    }
    console.log(JSON.stringify(result));
    return result.insertedId.toString();
    })
  .catch(err => {
    const str = `Failed to insert follow: ${err}`;
    console.error(str);
    return str;
  });
};
