exports = function(newComment){
  const challengeId = newComment["thread_id"];
  const commentCreatorId = newComment["creator_id"];
  const commentCreatorUsername = newComment["creator"];

  // get challenge creator
  let challenges = context.services.get("mongodb-atlas").db("challe").collection("challenges");

  const challengesQuery = {
    "is_deleted": false,
    "_id": challengeId
  };
  console.log(JSON.stringify(challengesQuery));

  return challenges.findOne(challengesQuery)
    .then(challengesResult => {
      const challengeCreatorId = challengesResult.creator_id;
      const challengeCreatorUsername = challengesResult.creator;

      // get challenge members
      let userChallenges = context.services.get("mongodb-atlas").db("challe").collection("user_challenges");

      const userChallengesQuery = {
        "is_deleted": false,
        "challenge_id": challengeId
      };

      const userChallengesPipeline = [
        {"$match": userChallengesQuery},
        {"$project": {
          "_id": 0,
          "creator_id": 1
         }
        }
      ];
      console.log(JSON.stringify(userChallengesPipeline));

      return userChallenges.aggregate(userChallengesPipeline)
        .toArray()
        .then(userChallengesResult => {
          console.log(JSON.stringify(userChallengesResult));
          const userChallengesResLength = userChallengesResult.length;

          let otherChallengeMembers = [];
          for (var i = 0; i < userChallengesResLength; i++) {

            const otherChallengeMemberId = userChallengesResult[i].creator_id;

            // don't alert self to own comment
            if(otherChallengeMemberId != commentCreatorId) {
              otherChallengeMembers.push(otherChallengeMemberId);
            }

          }

          // get comment creator followers
          const follows = context.services.get("mongodb-atlas").db("challe").collection("follows");

          let followsQuery = {
            "is_deleted": false,
            "is_approved": true,
            "following_id": commentCreatorId,
            "creator_id": {"$in": otherChallengeMembers}
          };

          let followsPipeline = [
            {"$match": followsQuery},
            {"$project": {
              "_id": 0,
              "creator_id": 1,
            }}
          ];
          console.log(JSON.stringify(followsPipeline));

          return follows.aggregate(followsPipeline)
            .toArray()
            .then(followsResult => {
              console.log(JSON.stringify(followsResult));
              const followsResLength = followsResult.length;

              let challengeMembeCommentCreatorFollowerIds = [];
              for (var i = 0; i < followsResLength; i++) {
                challengeMembeCommentCreatorFollowerIds.push(followsResult[i].creator_id);
              }

              // get comment creator followers who'll receive notification
              const uns = context.services.get("mongodb-atlas").db("challe").collection("user_notification_settings");

              let unsQuery = {
                "is_deleted": false,
                "receive_followed_user_commented_on_joined_challenge_notifs": true,
                "fcm_token": {"$exists": true},
                "creator_id": {"$in": challengeMembeCommentCreatorFollowerIds}
              };

              let unsPipeline = [
                {"$match": unsQuery},
                {"$project": {
                  "_id": 0,
                  "fcm_token": 1,
                }}
              ];
              console.log(JSON.stringify(unsPipeline));

              return uns.aggregate(unsPipeline)
                .toArray()
                .then(unsResult => {
                  console.log(JSON.stringify(unsResult));
                  const unsResLength = unsResult.length;

                  let notificationRecipientTokens = [];
                  for (var i = 0; i < unsResLength; i++) {
                    if(unsResult[i].fcm_token) {
                      notificationRecipientTokens.push(unsResult[i].fcm_token);
                    }
                  }

                  // send notification
                  if(notificationRecipientTokens.length > 0) {

                    return context.services.get("gcm").send({
                      "registrationTokens": notificationRecipientTokens,
                      "priority": "normal",
                      "notification": {
                          "body": commentCreatorUsername + " commented on " + challengeCreatorUsername + "'s challenge!",
                          "sound": "ping.aiff",
                      },
                      "data": {
                        "initial_view_identifier": "challengeViewController",
                        "challenge_id": String(challengeId) // sending as string easier for client json interpretation
                      }
                    });

                  } else {

                    return;

                  }

                })
                .catch(err => console.error(`Failed to get uns: ${err}`));

            })
            .catch(err => console.error(`Failed to find comment creator followers: ${err}`));

        })
        .catch(err => console.error(`Failed to find challenge members: ${err}`));

    })
    .catch(err => console.error(`Failed to find challenge: ${err}`));
};

// For testing:
// exports({
//   "_id": BSON.ObjectId("5cbb56904aa455c1110652a6"),
//   "creator_id":"5cbb50902f486889b7e48dc5",
//   "creator_image_url":"https://challe-userdata.s3.amazonaws.com/profile-photos/5cbb50902f486889b7e48dc5/1aab887a-35de-4874-b05b-133f894855ba.jpg",
//   "thread_type":"challenge",
//   "thread_id": BSON.ObjectId("5cb9289d2fd4997edb144792"),
//   "created_at":"2019-04-20T17:27:40.131Z",
//   "is_deleted":false,
//   "n_flags":"0",
//   "creator":"gloria",
//   "text":"I love this! Are you using an app for meditation?"
// })
