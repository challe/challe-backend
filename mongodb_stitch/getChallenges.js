exports = function(argsDoc, nPerPage=2, pageNumber=1){
  console.log(JSON.stringify(argsDoc));
  console.log(nPerPage);
  console.log(pageNumber);

  const currentUserId = context.user.id;
  console.log(currentUserId);

  if(argsDoc.follower) {
    const follower = argsDoc.follower;
    const completed = argsDoc.completed;

    return context.functions.execute("getFollowedChallengeIds", follower, completed)
      .then(resp => {
        console.log(JSON.stringify(resp));
        const followerChallengeIds = resp["followed"];
        return getChallenges(argsDoc, nPerPage, pageNumber, followerChallengeIds);

      });
  } else {

    return getChallenges(argsDoc, nPerPage, pageNumber);

  }

  function getChallenges(argsDoc, nPerPage, pageNumber, challengeIds=null) {
    return context.functions.execute("getFollowedChallengeIds", currentUserId)
      .then(resp => {
        console.log(JSON.stringify(resp));
        const followedChallengeIds = resp["followed"];
        const completedChallengeIds = resp["completed"];

        return context.functions.execute("getFollowedUserIds", currentUserId)
          .then(followedUserIds => {
            console.log(JSON.stringify(followedUserIds));

            const maxCreatedAt = argsDoc.maxCreatedAt;
            const minStartDate = argsDoc.minStartDate;
            const maxStartDate = argsDoc.maxStartDate;
            const creator = argsDoc.creator;
            const sortBy = argsDoc.sortBy;
            const sortOrder = argsDoc.sortOrder;

            const challenges = context.services.get("mongodb-atlas").db("challe").collection("challenges");

            let creatorFilt = {};
            if(creator == "any") {
              creatorFilt["$exists"] = true;
            } else if(creator == "following") {
              creatorFilt["$in"] = followedUserIds;
            } else {
              creatorFilt["$eq"] = creator;
            }

            let query = {
              "is_deleted": false,
              "n_flags": {"$lt": 3},
              "start_date": {
                "$gte": minStartDate,
                "$lte": maxStartDate
              },
              "created_at": {"$lte": maxCreatedAt},
              "$or": [
                {"visibility": "Everyone"},
                {"$and": [
                  {"visibility": "My followers"},
                  {"creator_id": {"$in": followedUserIds}}
                ]},
                {"$and": [
                  {"visibility": "Link sharing only"},
                  {"$or": [
                    {"creator_id": currentUserId},
                    {"_id": {"$in": followedChallengeIds}},
                    {"_id": {"$in": completedChallengeIds}}
                  ]}
                ]}
              ],
              "creator_id": creatorFilt
            };


            if(challengeIds) {
              query["_id"] = {"$in": challengeIds};
            }

            let sort = {};
            sort[sortBy] = sortOrder;

            const pipeline = [
              {"$match": query},
              {"$sort": sort},
              {"$skip": (pageNumber > 0 ? ( ( pageNumber - 1 ) * nPerPage ) : 0)},
              {"$limit": nPerPage}
            ];
            console.log(JSON.stringify(pipeline));

            return challenges.aggregate(pipeline)
              .toArray()
              .then(result => {
                const resLength = result.length;

                let followedChallengeIdStrs = [];
                if(followedChallengeIds) {
                  followedChallengeIdStrs = followedChallengeIds.map(String);
                }

                let completedChallengeIdStrs = [];
                if(completedChallengeIds) {
                  completedChallengeIdStrs = completedChallengeIds.map(String);
                }

                for (var i = 0; i < resLength; i++) {
                  const challengeId = String(result[i]._id); // indexOf doesn't work with mongo object arrays
                  const creatorId = result[i].creator_id;

                  if(followedChallengeIds) {
                    result[i].user_is_follower = (followedChallengeIdStrs.indexOf(challengeId) > -1);
                  } else {
                    result[i].user_is_follower = false;
                  }

                  if(completedChallengeIds) {
                    result[i].user_has_completed = (completedChallengeIdStrs.indexOf(challengeId) > -1);
                  } else {
                    result[i].user_has_completed = false;
                  }

                  if(followedUserIds) {
                    result[i].user_follows_creator = (followedUserIds.indexOf(creatorId) > -1);
                  } else {
                    result[i].user_follows_creator = false;
                  }
                }

                console.log(JSON.stringify(result));
                return result;

              })
              .catch(err => console.error(`Failed to find challenges: ${err}`));

          })
          .catch(err => console.error(`Failed to find followed user ids: ${err}`));

      })
      .catch(err => console.error(`Failed to find active challenge ids: ${err}`));
}

};


// // FOR TESTING:
// let maxCreatedAt = new Date("2019-03-15")
// let minStartDate = new Date("2019-03-01")
// let maxStartDate = new Date("2019-03-31")

// exports(
//   {"maxCreatedAt": maxCreatedAt,
//   "minStartDate": minStartDate,
//   "maxStartDate": maxStartDate,
//   "creator": "any",
//   "sortField": "created_at"
//   }, 2, 1)
