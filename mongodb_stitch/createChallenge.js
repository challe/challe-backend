exports = function(newChallenge){
  console.log(JSON.stringify(newChallenge));

  let challenges = context.services.get("mongodb-atlas").db("challe").collection("challenges");

  return challenges.insertOne(newChallenge)
  .then(result => {
    console.log(JSON.stringify(result));

    const challengeId = result.insertedId;
    newChallenge["_id"] = challengeId;

    if(["Everyone", "My followers"].indexOf(newChallenge["visibility"]) >= 0) {
      context.functions.execute("sendFollowedUserCreatedChallengeNotifications", newChallenge);
    }

    return challengeId.toString();
    })
  .catch(err => {
    const str = `Failed to insert challenge: ${err}`;
    console.error(str);
    return str;
  });
};
