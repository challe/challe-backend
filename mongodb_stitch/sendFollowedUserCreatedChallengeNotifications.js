exports = function(newChallenge){
  const challengeId = newChallenge["_id"];
  const challengeCreator = newChallenge["creator"];
  const challengeCreatorId = newChallenge["creator_id"];

  // get creator followers
  const follows = context.services.get("mongodb-atlas").db("challe").collection("follows");

  let followsQuery = {
    "is_deleted": false,
    "is_approved": true,
    "following_id": challengeCreatorId
  };

  let followsPipeline = [
    {"$match": followsQuery},
    {"$project": {
      "_id": 0,
      "creator_id": 1,
    }}
  ];
  console.log(JSON.stringify(followsPipeline));

  return follows.aggregate(followsPipeline)
    .toArray()
    .then(followsResult => {
      console.log(JSON.stringify(followsResult));
      const followsResLength = followsResult.length;

      let creatorFollowerIds = [];
      for (var i = 0; i < followsResLength; i++) {
        creatorFollowerIds.push(followsResult[i].creator_id);
      }

      // get followers who'll receive notification
      const uns = context.services.get("mongodb-atlas").db("challe").collection("user_notification_settings");

      let unsQuery = {
        "is_deleted": false,
        "receive_followed_user_created_challenge_notifs": true,
        "fcm_token": {"$exists": true},
        "creator_id": {"$in": creatorFollowerIds}
      };

      let unsPipeline = [
        {"$match": unsQuery},
        {"$project": {
          "_id": 0,
          "fcm_token": 1,
        }}
      ];
      console.log(JSON.stringify(unsPipeline));

      return uns.aggregate(unsPipeline)
      .toArray()
      .then(unsResult => {
        console.log(JSON.stringify(unsResult));
        const unsResLength = unsResult.length;

        let notificationRecipientTokens = [];
        for (var i = 0; i < unsResLength; i++) {
          if(unsResult[i].fcm_token) {
            notificationRecipientTokens.push(unsResult[i].fcm_token);
          }
        }

        // send notification
        if(notificationRecipientTokens.length > 0) {

          return context.services.get("gcm").send({
            "registrationTokens": notificationRecipientTokens,
            "priority": "normal",
            "notification": {
                "body": challengeCreator + " created a new challenge!",
                "sound": "ping.aiff",
            },
            "data": {
              "initial_view_identifier": "challengeViewController",
              "challenge_id": String(challengeId) // sending as string easier for client json interpretation
            }
          });

        } else {

          return;

        }

      })
      .catch(err => console.error(`Failed to get uns: ${err}`));

    })
    .catch(err => console.error(`Failed to find creator followers: ${err}`));
};

// testing
// exports({
//   "_id": BSON.ObjectId("5cae96b7619761ff7a98500c"),
//   "creator_id":"5cadf20df57dc08a6fd3667a",
//   "creator": "phillyphilly"
// })
