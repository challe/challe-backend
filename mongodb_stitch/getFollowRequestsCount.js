exports = function(){
  const currentUserId = context.user.id;
  console.log(currentUserId);

  const follows = context.services.get("mongodb-atlas").db("challe").collection("follows");

  let query = {
    "is_deleted": false,
    "is_approved": false,
    "following_id": currentUserId
  };
  console.log(JSON.stringify(query));

  return follows.count(query)
    .then(result => {

      console.log(JSON.stringify(result));
      return result;

    })
    .catch(err => console.error(`Failed to find challenges: ${err}`));


};
