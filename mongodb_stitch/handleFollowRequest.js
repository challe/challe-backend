exports = function(followingIdStr, followerIdStr, approve){
  let follows = context.services.get("mongodb-atlas").db("challe").collection("follows");

  const query = {
    "creator_id": followerIdStr,
    "following_id": followingIdStr,
    "is_deleted": false
  };
  console.log(JSON.stringify(query));

  let update;
  if(approve) {
    update = {
      "$set": { "is_approved": true }
    };
  } else {
    const now = new Date();
    update = {
      "$set": { "is_deleted": true,  "deleted_at": now }
    };
  }
  console.log(JSON.stringify(update));

  const options = { upsert: false };
  console.log(JSON.stringify(options));

  return follows.updateMany(query, update, options)
  .then(result => {
    console.log(JSON.stringify(result));

    if(approve) {
      context.functions.execute("updateNFollowing", "users", BSON.ObjectId(followerIdStr), 1);
      context.functions.execute("updateNFollowers", "users", BSON.ObjectId(followingIdStr), 1);
      context.functions.execute("sendProfileFollowRequestApprovedNotification", followingIdStr, followerIdStr);

      return "Successfully approved follow request.";
    } else {
      return "Successfully rejected follow request.";
    }
    })
  .catch(err => {
    const str = `Failed to update follow request: ${err}`;
    console.error(str);
    return str;
  });
};
