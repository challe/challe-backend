exports = function(newUser){
  console.log(JSON.stringify(newUser));

  let users = context.services.get("mongodb-atlas").db("challe").collection("users");

  const userId = newUser._id;

  return users.count({"_id": userId})
    .then(numUsers => {
      console.log(numUsers);
      if(numUsers === 0) {
        return users.insertOne(newUser)
          .then(result => {
            console.log(JSON.stringify(result));
            return result.insertedId.toString();
            })
          .catch(err => {
            const str = `Failed to insert item: ${err}`;
            console.error(str);
            return str;
          });
      } else {
        const str = "User already exists.";
        console.log(str);
        return str;
      }
    })
    .catch(err => {
      const str = `Failed to count users: ${err}`;
      console.error(str);
      return str;
    });
};
