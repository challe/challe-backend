exports = function(creatorId){
  let userNotificationSettings = context.services.get("mongodb-atlas").db("challe").collection("user_notification_settings");

  const query = {
    "is_deleted": false,
    "creator_id": creatorId
  };

  console.log(JSON.stringify(query));

  return userNotificationSettings.find(query)
    .toArray()
    .then(result => {
      console.log(JSON.stringify(result));
      return result[0];
    })
    .catch(err => console.error(`Failed to find userChallenge: ${err}`));
};
