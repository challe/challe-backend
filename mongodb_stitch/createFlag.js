exports = function(newFlag){

  console.log(JSON.stringify(newFlag));

  let flags = context.services.get("mongodb-atlas").db("challe").collection("flags");

  let creatorId = newFlag.creator_id;
  let targetType = newFlag.target_type;
  let targetId = newFlag.target_id;

  let query = {
    "target_type": targetType,
    "target_id": targetId,
    "creator_id": creatorId
  };

  console.log(JSON.stringify(query));

  return flags.count(query)
    .then(numFlags => {
      console.log(numFlags);

      if(numFlags === 0) {
        return flags.insertOne(newFlag)
          .then(result => {
            context.functions.execute("updateNFlags", targetType+"s", targetId);
            console.log(JSON.stringify(result));
            return "Stitch created flag with ID: " + result.insertedId.toString();
            })
          .catch(err => {
            const str = `Failed to insert item: ${err}`;
            console.error(str);
            return str;
          });
      } else {
        const str = "Flag already exists.";
        console.log(str);
        return str;
      }

    })
    .catch(err => {
      const str = `Failed to count flags: ${err}`;
      console.error(str);
      return str;
    });
};
