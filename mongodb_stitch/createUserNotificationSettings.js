exports = function(newUNS){
  console.log(JSON.stringify(newUNS));

  let userNotificationSettings = context.services.get("mongodb-atlas").db("challe").collection("user_notification_settings");

  const userId = newUNS.creator_id;

  // patches
  // version < 1.0.4
  if(!newUNS["receive_new_challenge_joiner_notifs"]) {
    newUNS["receive_daily_challenge_digests_at_utc_hour"] = parseInt(newUNS["receive_daily_challenge_digests_at_utc_hour"], 10) // convert to int64
    newUNS["fcm_token"] = "";
  	newUNS["receive_new_challenge_joiner_notifs"] = true;
  	newUNS["receive_new_challenge_comment_notifs"] = true;
  	newUNS["receive_new_profile_follower_notifs"] = true;
  	newUNS["receive_profile_follow_request_notifs"] = true;
  	newUNS["receive_profile_follow_request_approved_notifs"] = true;
    newUNS["receive_followed_user_created_challenge_notifs"] = true;
  }

  // version < 1.0.5
  if(!newUNS["receive_followed_user_commented_on_joined_challenge_notifs"]) {
    newUNS["receive_followed_user_commented_on_joined_challenge_notifs"] = false;
  }

  // version <= 1.0.5 bug where V.fcmToken is not re-populated during logout --> verify email --> login
  newUNS["last_known_fcm_token"] = newUNS["fcm_token"];

  // version < 1.0.6
  if(!newUNS["receive_challenge_reminders_default"]) {
    newUNS["receive_challenge_reminders_default"] = true;
    newUNS["receive_challenge_reminders_at_utc_hour_default"] = newUNS["receive_daily_challenge_digests_at_utc_hour"];
  }

  return userNotificationSettings.count({"creator_id": userId})
    .then(numUsers => {
      console.log(numUsers);
      if(numUsers === 0) {
        return userNotificationSettings.insertOne(newUNS)
          .then(result => {
            console.log(JSON.stringify(result));
            return result.insertedId.toString();
            })
          .catch(err => {
            const str = `Failed to insert item: ${err}`;
            console.error(str);
            return str;
          });
      } else {
        const str = "User already exists.";

        const toSet = {
          "apn_token": newUNS.apn_token,
          "fcm_token": newUNS.fcm_token
        };

        return context.functions.execute("updateUserNotificationSettings", userId, toSet)
          .then(result => {
            console.log(JSON.stringify(result));
            return result;
            })
          .catch(err => {
            console.error(str);
            return str;
          });
      }
    })
    .catch(err => {
      const str = `Failed to count users: ${err}`;
      console.error(str);
      return str;
    });
};
