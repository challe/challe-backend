exports = function(challengeId, creatorId){
  let userChallenges = context.services.get("mongodb-atlas").db("challe").collection("user_challenges");

  const query = {
    "is_deleted": false,
    "challenge_id": challengeId,
    "creator_id": creatorId
  };

  console.log(JSON.stringify(query));

  return userChallenges.find(query)
    .toArray()
    .then(result => {
      console.log(JSON.stringify(result));
      return result;
    })
    .catch(err => console.error(`Failed to find userChallenge: ${err}`));
};
