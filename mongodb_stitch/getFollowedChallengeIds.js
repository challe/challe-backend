exports = function(creatorId, completed=null){

  const userChallenges = context.services.get("mongodb-atlas").db("challe").collection("user_challenges");

  const query = {
    "is_deleted": false,
    "creator_id": creatorId
  };

  let pipeline = [
    {"$match": query},
    {"$project": {
      "_id": 0,
      "challenge_id": 1,
      "is_complete": {"$eq": ["$n_subchallenges", "$n_completed_subchallenges"]}
      }
    }
  ];

  if(completed === true) {
    pipeline.push({"$match": {"is_complete": true}});
  } else if(completed === false) {
    pipeline.push({"$match": {"is_complete": false}});
  }

  console.log(JSON.stringify(pipeline));

  return userChallenges.aggregate(pipeline)
    .toArray()
    .then(result => {
      let ids = [];
      let completed_ids = [];

      const resLength = result.length;
      for (var i = 0; i < resLength; i++) {
        ids.push(result[i].challenge_id);
        if(result[i].is_completed) {
          completed_ids.push(result[i].challenge_id);
        }
      }
      console.log(JSON.stringify(result));
      console.log(JSON.stringify(ids));
      console.log(JSON.stringify(completed_ids));

      return {"followed": ids, "completed": completed_ids};
    })
    .catch(err => console.error(`Failed to find any user challenges: ${err}`));
};
