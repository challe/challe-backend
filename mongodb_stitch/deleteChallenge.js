exports = function(challengeId){

  let challenges = context.services.get("mongodb-atlas").db("challe").collection("challenges");

  const query = {"_id": challengeId};
  console.log(JSON.stringify(query));

  const now = new Date();
  const update = {
    "$set": {
        "is_deleted": true,
        "deleted_at": now
    }
  };
  console.log(JSON.stringify(update));

  const options = { upsert: false };
  console.log(JSON.stringify(options));

  return challenges.updateOne(query, update, options)
  .then(result => {
    console.log(JSON.stringify(result));
    const { matchedCount, modifiedCount } = result;
    if(matchedCount && modifiedCount) {
      return `Successfully deleted challenge.`;
    }
  })
  .catch(err => console.error(`Failed to delete challenge: ${err}`));
};
