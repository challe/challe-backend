exports = function(challengeId, creatorId, subChallengeIdx, updatedSubChallengeProgression){

  let userChallenges = context.services.get("mongodb-atlas").db("challe").collection("user_challenges");

  const query = {
    "is_deleted": false,
    "challenge_id": challengeId,
    "creator_id": creatorId
  };
  console.log(JSON.stringify(query));

  let update = {"$set": {}};
  update.$set["user_progression." + subChallengeIdx] = updatedSubChallengeProgression;

  if(updatedSubChallengeProgression.is_complete) {
    update["$inc"] = {"n_completed_subchallenges": parseInt(1, 10)};
  } else {
    update["$inc"] = {"n_completed_subchallenges": parseInt(-1, 10)};
  }
  console.log(JSON.stringify(update));

  const options = { upsert: false };
  console.log(JSON.stringify(options));

  return userChallenges.updateOne(query, update, options)
  .then(result => {
    console.log(JSON.stringify(result));
    const { matchedCount, modifiedCount } = result;
    if(matchedCount && modifiedCount) {
      return `Successfully updated user challenge.`;
    }
  })
  .catch(err => console.error(`Failed to update user challenge: ${err}`));
};
