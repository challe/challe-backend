exports = function(followingId, creatorId){

  let follows = context.services.get("mongodb-atlas").db("challe").collection("follows");

  const query = {
    "is_deleted": false,
    "following_id": followingId,
    "creator_id": creatorId
  };
  console.log(JSON.stringify(query));

  const now = new Date();
  const update = {
    "$set": {
        "is_deleted": true,
        "deleted_at": now
    }
  };
  console.log(JSON.stringify(update));

  const options = { upsert: false };
  console.log(JSON.stringify(options));

  return follows.updateOne(query, update, options)
  .then(result => {
    console.log(JSON.stringify(result));
    const { matchedCount, modifiedCount } = result;
    if(matchedCount && modifiedCount) {
      context.functions.execute("updateNFollowing", "users", BSON.ObjectId(creatorId), -1);
      context.functions.execute("updateNFollowers", "users", BSON.ObjectId(followingId), -1);
      return `Successfully deleted follow.`;
    }
  })
  .catch(err => console.error(`Failed to delete follow: ${err}`));
};
