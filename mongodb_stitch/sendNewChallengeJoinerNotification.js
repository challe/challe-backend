exports = function(newUserChallenge){
  const challengeId = newUserChallenge["challenge_id"];
  const followerId = newUserChallenge["creator_id"];

  // get challenge creator
  let challenges = context.services.get("mongodb-atlas").db("challe").collection("challenges");

  const challengesQuery = {
    "is_deleted": false,
    "_id": challengeId
  };
  console.log(JSON.stringify(challengesQuery));

  return challenges.findOne(challengesQuery)
    .then(challengesResult => {
      // do nothing if self
      const challengeCreatorId = challengesResult["creator_id"];

      if(challengeCreatorId == newUserChallenge["creator_id"]) {

        return;

      } else {

        // get challenge creator's notification settings
        let uns = context.services.get("mongodb-atlas").db("challe").collection("user_notification_settings");

        const unsQuery = {
          "is_deleted": false,
          "creator_id": challengeCreatorId
        };
        console.log(JSON.stringify(unsQuery));

        return uns.findOne(unsQuery)
          .then(unsResult => {

            if(unsResult["receive_new_challenge_joiner_notifs"] && unsResult["fcm_token"]) {

              // get challenge follower's username
              let users = context.services.get("mongodb-atlas").db("challe").collection("users");

              const usersQuery = {
                "is_deleted": false,
                "_id": BSON.ObjectId(followerId)
              };
              console.log(JSON.stringify(usersQuery));

              return users.findOne(usersQuery)
                .then(usersResult => {

                  return context.services.get("gcm").send({
                    "registrationTokens": [unsResult["fcm_token"]],
                    "priority": "normal",
                    "notification": {
                        "body": usersResult["username"] + " joined your challenge!",
                        "sound": "ping.aiff",
                    },
                    "data": {
                      "initial_view_identifier": "challengeViewController",
                      "challenge_id": String(challengeId) // sending as string easier for client json interpretation
                    }
                 });
                })
                .catch(err => console.error(`Failed to find user: ${err}`));

            } else {

              return;

            }

          })
          .catch(err => console.error(`Failed to find uns: ${err}`));

      }

    })
    .catch(err => console.error(`Failed to find challenge: ${err}`));
};

// For testing:
// exports({
// "creator_id": "5caa9e93b983934c9e7231ab",
// "challenge_id": BSON.ObjectId("5cab338c7211c579f339ba2a")
// })
