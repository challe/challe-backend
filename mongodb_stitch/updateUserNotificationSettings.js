exports = function(creatorId, toSet){

  return context.functions.execute("getUserNotificationSettings", creatorId)
    .then(currentUNS => {

      let userNotificationSettings = context.services.get("mongodb-atlas").db("challe").collection("user_notification_settings");

      const query = {
        "is_deleted": false,
        "creator_id": creatorId
      };
      console.log(JSON.stringify(query));

      let update = {};

      // patch for keeping version < 1.0.6 edits synced
      if(toSet.hasOwnProperty('receive_daily_challenge_digests')) {
        toSet['receive_challenge_reminders_default'] = toSet['receive_daily_challenge_digests'];
      }
      if(toSet.hasOwnProperty('receive_daily_challenge_digests_at_utc_hour')) {
        toSet['receive_challenge_reminders_at_utc_hour_default'] = toSet['receive_daily_challenge_digests_at_utc_hour'];
      }

      // version <= 1.0.5 bug where V.fcmToken is not re-populated during logout --> verify email --> login
      if(toSet["apn_token"] && toSet["fcm_token"]) {
        toSet["last_known_fcm_token"] = toSet["fcm_token"];
      } else if(toSet["apn_token"] && !toSet["fcm_token"] && currentUNS["last_known_fcm_token"]){
        toSet["fcm_token"] = currentUNS["last_known_fcm_token"];
      }

      if(toSet["timezone_delta"]) {

        const currentRDCDAUH = currentUNS["receive_daily_challenge_digests_at_utc_hour"];
        let newRDCDAUH = parseInt(((((currentRDCDAUH + toSet["timezone_delta"]) % 24) + 24) % 24), 10);
        toSet["receive_daily_challenge_digests_at_utc_hour"] = newRDCDAUH;

        const currentRCRUHD = currentUNS["receive_challenge_reminders_at_utc_hour_default"];
        let newRCRUHD = parseInt(((((currentRCRUHD + toSet["timezone_delta"]) % 24) + 24) % 24), 10);
        toSet["receive_challenge_reminders_at_utc_hour_default"] = newRCRUHD;

        delete toSet["timezone_delta"];

      }

      update["$set"] = toSet;
      console.log(JSON.stringify(update));

      const options = { upsert: false };
      console.log(JSON.stringify(options));

      return userNotificationSettings.updateOne(query, update, options)
      .then(result => {
        console.log(JSON.stringify(result));
        const { matchedCount, modifiedCount } = result;
        if(matchedCount && modifiedCount) {
          return `Successfully updated user notification settings.`;
        }
      })
      .catch(err => console.error(`Failed to update user notification settings: ${err}`));

    })
    .catch(err => console.error(`Failed to coll user notification settings: ${err}`));
};
