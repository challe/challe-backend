exports = function(threadType, threadId, maxCreatedAt, nPerPage=2, pageNumber=0){
  let comments = context.services.get("mongodb-atlas").db("challe").collection("comments");

  const query = {
    "is_deleted": false,
    "n_flags": {"$lt": 3},
    "thread_type": threadType,
    "created_at": {"$lte": maxCreatedAt},
    "thread_id": threadId
  };

  const pipeline = [
    {"$match": query},
    {"$sort": {"created_at": -1}},
    {"$skip": (pageNumber > 0 ? ( ( pageNumber - 1 ) * nPerPage ) : 0)},
    {"$limit": nPerPage}
  ];
  console.log(JSON.stringify(pipeline));

  return comments.aggregate(pipeline)
    .toArray()
    .then(result => {
      console.log(JSON.stringify(result));
      return result;
    })
    .catch(err => console.error(`Failed to find document: ${err}`));
};
