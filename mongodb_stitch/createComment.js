exports = function(newComment){
  console.log(JSON.stringify(newComment));

  let comments = context.services.get("mongodb-atlas").db("challe").collection("comments");

  return comments.insertOne(newComment)
  .then(commentsResult => {
    console.log(JSON.stringify(commentsResult));

    context.functions.execute("sendNewChallengeCommentNotification", newComment);
    context.functions.execute("sendFollowedUserCommentedOnJoinedChallengeNotifications", newComment);

    return commentsResult.insertedId.toString();
    })
  .catch(err => {
    const str = `Failed to insert item: ${err}`;
    console.error(str);
    return str;
  });
};
