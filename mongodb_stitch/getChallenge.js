exports = function(objectId){
  const thisUser = context.user.id;
  console.log(thisUser);

  return context.functions.execute("getFollowedChallengeIds", thisUser)
    .then(resp => {
      console.log('followed challenge ids: ', JSON.stringify(resp));
      const followedChallengeIds = resp["followed"];
      const completedChallengeIds = resp["completed"];

      return context.functions.execute("getFollowedUserIds", thisUser)
      .then(followedUserIds => {
        console.log('followed user ids: ', JSON.stringify(followedUserIds));

        let challenges = context.services.get("mongodb-atlas").db("challe").collection("challenges");

        const query = {
          "is_deleted": false,
          "_id": objectId
        };
        console.log(JSON.stringify(query));

        return challenges.findOne(query)
          .then(result => {
            const challengeId = String(result._id); // indexOf doesn't work with mongo object arrays
            const creatorId = result.creator_id;

            if(followedChallengeIds) {
              result.user_is_follower = (followedChallengeIds.map(String).indexOf(challengeId) > -1);
            } else {
              result.user_is_follower = false;
            }

            if(completedChallengeIds) {
              result.user_has_completed = (completedChallengeIds.map(String).indexOf(challengeId) > -1);
            } else {
              result.user_has_completed = false;
            }

            if(followedUserIds) {
              result.user_follows_creator = (followedUserIds.indexOf(creatorId) > -1);
            } else {
              result.user_follows_creator = false;
            }

            console.log(JSON.stringify(result));
            return result;
          })
          .catch(err => console.error(`Failed to find challenge: ${err}`));

      })
      .catch(err => console.error(`Failed to find followed user ids: ${err}`));

    })
    .catch(err => console.error(`Failed to find active challenge ids: ${err}`));
};
