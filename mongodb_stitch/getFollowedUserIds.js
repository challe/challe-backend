exports = function(creatorId, includeSelf=true, includePending=false, returnApproval=false){

  const follows = context.services.get("mongodb-atlas").db("challe").collection("follows");

  let query = {
    "is_deleted": false,
    "is_approved": true,
    "creator_id": creatorId
  };

  if(includePending) {
   query["is_approved"] = {"$exists": true};
  }

  let pipeline = [
    {"$match": query},
    {"$project": {
      "_id": 0,
      "following_id": 1,
    }}
  ];

  if(returnApproval) {
    pipeline[1]["$project"]["is_approved"] = 1;
  }

  console.log(JSON.stringify(pipeline));

  return follows.aggregate(pipeline)
    .toArray()
    .then(result => {
      console.log(JSON.stringify(result));
      const resLength = result.length;

      let ids = [];

      const currentUserId = context.user.id;
      console.log(currentUserId);

      if(returnApproval) {

        for (var i = 0; i < resLength; i++) {
          ids.push(result[i]);
        }

        if(includeSelf){
          ids.push({"following_id": currentUserId, "is_approved": true});
        }

      } else {

        for (var i = 0; i < resLength; i++) {
          ids.push(result[i].following_id);
        }

        if(includeSelf){
          ids.push(currentUserId);
        }

      }

      console.log(JSON.stringify(ids));
      return ids;
    })
    .catch(err => console.error(`Failed to find any followed users: ${err}`));
};
