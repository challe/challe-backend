exports = function(followingIdStr, followerIdStr){
  // get following username
  let users = context.services.get("mongodb-atlas").db("challe").collection("users");

  const usersQuery = {
    "is_deleted": false,
    "_id": BSON.ObjectId(followingIdStr)
  };
  console.log(JSON.stringify(usersQuery));

  return users.findOne(usersQuery)
    .then(usersResult => {
      // get follower's notification settings
      let uns = context.services.get("mongodb-atlas").db("challe").collection("user_notification_settings");

      const unsQuery = {
        "is_deleted": false,
        "creator_id": followerIdStr
      };
      console.log(JSON.stringify(unsQuery));

      return uns.findOne(unsQuery)
        .then(unsResult => {

          if(unsResult["receive_profile_follow_request_approved_notifs"] && unsResult["fcm_token"]) {

            return context.services.get("gcm").send({
              "registrationTokens": [unsResult["fcm_token"]],
              "priority": "normal",
              "notification": {
                  "body": usersResult["username"] + " approved your follow request!",
                  "sound": "ping.aiff",
              },
              "data": {
                "initial_view_identifier": "otherProfileViewController",
                "challe_user_id": followingIdStr
              }
           });

         } else {

           return

         }

        })
        .catch(err => console.error(`Failed to find uns: ${err}`));

    })
    .catch(err => console.error(`Failed to find user: ${err}`));
};

// For testing:
// exports("5caa9e93b983934c9e7231ab", "5caa96ba082bf6ab87520766")
