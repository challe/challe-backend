function (user, context, callback) {
  if (context.request.geoip) {
    var country = context.request.geoip.country_name;
    var timezone = context.request.geoip.time_zone;
    // add to idToken
    context.idToken['https://www.challe.co/country'] = country;
    context.idToken['https://www.challe.co/timezone'] = timezone;

    // update user metadata and return
    user.user_metadata = user.user_metadata || {};
    user.user_metadata.country = country;
    user.user_metadata.timezone = timezone;
    auth0.users.updateUserMetadata(user.user_id, user.user_metadata)
    .then(function(){
      callback(null, user, context);
    })
    .catch(function(err){
      callback(err);
    });
  }
}
