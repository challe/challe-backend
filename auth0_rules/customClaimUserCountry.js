function (user, context, callback) {
    var namespace = 'https://www.challe.co/'; // note that you cannot use auth0.com, webtask.io and webtask.run as a namespace identifier
    if (context.idToken) {
       if (user.user_metadata.country) {
          context.idToken[namespace + 'country'] = user.user_metadata.country;
       } else {
          // patch case when geoip is unknown
          context.idToken[namespace + 'country'] = 'United States';
       }
    }
    callback(null, user, context);
 }
