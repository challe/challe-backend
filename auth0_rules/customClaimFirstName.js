function (user, context, callback) {
    var namespace = 'https://www.challe.co/'; // note that you cannot use auth0.com, webtask.io and webtask.run as a namespace identifier
    if (context.idToken && user.user_metadata.first_name) {
       context.idToken[namespace + 'first_name'] = user.user_metadata.first_name;
     }
   callback(null, user, context);
 }
