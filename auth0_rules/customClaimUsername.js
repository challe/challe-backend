function (user, context, callback) {
    var namespace = 'https://www.challe.co/'; // note that you cannot use auth0.com, webtask.io and webtask.run as a namespace identifier
    if (context.idToken && user.username) {
       context.idToken[namespace + 'username'] = user.username;
     }
   callback(null, user, context);
 }
