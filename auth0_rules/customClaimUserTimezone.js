function (user, context, callback) {
    var namespace = 'https://www.challe.co/'; // note that you cannot use auth0.com, webtask.io and webtask.run as a namespace identifier
    if (context.idToken) {
       if (user.user_metadata.timezone) {
          context.idToken[namespace + 'timezone'] = user.user_metadata.timezone;
       } else {
          // patch case where geoip is unknown
          context.idToken[namespace + 'timezone'] = 'America/New_York';
       }
     }
    callback(null, user, context);
 }
