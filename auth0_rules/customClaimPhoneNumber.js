function (user, context, callback) {
    var namespace = 'https://www.challe.co/'; // note that you cannot use auth0.com, webtask.io and webtask.run as a namespace identifier
    if (context.idToken && user.user_metadata.phone_number) {
       context.idToken[namespace + 'phone_number'] = user.user_metadata.phone_number;
     }
   callback(null, user, context);
 }
