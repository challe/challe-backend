function (user, context, callback) {
    var namespace = 'https://www.challe.co/'; // note that you cannot use auth0.com, webtask.io and webtask.run as a namespace identifier
    if (context.idToken && user.user_metadata.picture) {
       context.idToken[namespace + 'picture'] = user.user_metadata.picture;
     }
   callback(null, user, context);
 }
