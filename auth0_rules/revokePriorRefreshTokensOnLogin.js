function (user, context, callback) {
  if(context.protocol === "oauth2-refresh-token") {
    console.log(`Skipping refresh token deletion on refresh`);
    callback(null, user, context);
  } else {

    const apiURL = 'https://challe.auth0.com/api/v2/device-credentials';
    var accessToken;

    var async   = require('async');
    var request = require("request");
    var _       = require("underscore");

    async.waterfall([
      // STEP 1: obtain access_token to call Management API v2
      function get_access_token(cb){
        console.log('Getting access token...');
        var options = { method: 'POST',
            url: 'https://challe.auth0.com/oauth/token',
            json: {
              client_id: configuration.BACKEND_CLIENT_ID,
              client_secret: configuration.BACKEND_CLIENT_SECRET,
              audience: 'https://challe.auth0.com/api/v2/',
              grant_type: 'client_credentials'
            }
        };
        request(options, function (error, response, body) {
          if (error) throw new Error(error);
          accessToken = body.access_token;
          cb(null, accessToken);
        });
      },
      // STEP 2: GET device_credentials via Management API v2
      function get_device_credentials(accessToken, cb){
        console.log('Getting device credentials...');
        var options = {
            url: apiURL,
            method: 'GET',
            headers: {
              'content-type': 'application/json',
              'Authorization': 'Bearer ' + accessToken
            },
            qs: {
              type: 'refresh_token',
              user_id: user.user_id
            }
          };

        request(options, function(error, response, body){
          if (error) throw new Error(error);
          cb(null, body);
        });
      },
      // STEP 3: delete device credentials
      function delete_all_device_credentials(body, cb){
        console.log('Deleting refresh tokens...');
        console.log(`Body: ${body}`);
        _.each(JSON.parse(body), function(device_credential){
          console.log('deleting device credential ', device_credential.id);
          var options = {
            method: 'DELETE',
            url: apiURL + '/' + device_credential.id,
            headers: {
              'Authorization': 'Bearer ' + accessToken
            }
          };

          request(options, function(error, response, body){
              if(error) throw new Error(error);
              console.log('REFRESH TOKEN DELETED ', device_credential.id);
          });
        });

         callback(null, user, context);
      }

    ]);
  }
}
