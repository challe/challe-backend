"use strict";

var AWS = require('aws-sdk');
var secretsManagerClient = new AWS.SecretsManager({
    region: "us-east-1"
});
var apnSecretName = "apn/challe_backend/creds";
var apnSecret;
var mongoSecretName = "mongodb/challe_backend/creds";
var mongoSecret;

const apn = require('apn');
const moment = require('moment-timezone');
const MongoClient = require('mongodb').MongoClient;

let cachedConn = null;


module.exports.handler = (event, context, callback) => {
  context.callbackWaitsForEmptyEventLoop = false;

  console.log('event: ', event);

  secretsManagerClient.getSecretValue({SecretId: mongoSecretName}, function(err, data) {
    if(err) {
      throw err;
    } else {
      mongoSecret = JSON.parse(data.SecretString);

      secretsManagerClient.getSecretValue({SecretId: apnSecretName}, function(err, data) {
        if(err) {
          throw err;
        } else {
          apnSecret = JSON.parse(data.SecretString);

          connectToMongoDatabase(mongoSecret.MONGODB_ATLAS_CLUSTER_URI)
            .then(conn => sendDailyDigests(
              conn, event.production, apnSecret.APN_KEY, apnSecret.APN_KEY_ID,
              apnSecret.APN_TEAM_ID, apnSecret.APN_BUNDLE_ID
            ))
            .then(result => {
              console.log('=> returning result: ', result);
              callback(null, {"statusCode": 200, "body": JSON.stringify(result)});
            })
            .catch(err => {
              console.log('=> an error occurred upon getting digest recipients: ', err);
              const body = {"update_complete": false};
              callback(null, {"statusCode": 200, "body": JSON.stringify(body)});
            });
        }
      });
    }
  });

};


function connectToMongoDatabase (uri) {
    console.log('=> connect to database');

    if (cachedConn) {
        console.log('=> using cached database instance');
        return Promise.resolve(cachedConn);
    }

    return MongoClient.connect(uri)
      .then(conn => {
        cachedConn = conn;
        return cachedConn;
      });
}

async function sendDailyDigests(conn, production, key, keyId, teamId, bundleId) {

  let unsCursor = await getDigestRecipients(conn);

  const apnOptions = {
    token: {
      key: '\n-----BEGIN PRIVATE KEY-----\n'+key+'\n-----END PRIVATE KEY-----',
      keyId: keyId,
      teamId: teamId
    },
    production: production,
    connectionRetryLimit: 1
  };

  const apnProvider = new apn.Provider(apnOptions);

  let dailyDigest = new apn.Notification();
  dailyDigest.badge = 0;
  dailyDigest.payload = {'initial_view_identifier': 'ownProfileViewController'};
  dailyDigest.sound = "ping.aiff";
  dailyDigest.topic = bundleId;

  let nSuccessfulSends = 0;
  let nFailedSends = 0;

  while(await unsCursor.hasNext()) {

    const unsDoc = await unsCursor.next();
    console.log('=> user notification settings: ', unsDoc);

    const nSubChallengesDue = await getNSubChallengesDue(conn, unsDoc.creator_id, unsDoc.timezone);
    console.log('=> nChallengeStepsDue: ', nSubChallengesDue);

    if(nSubChallengesDue > 0) {
      if (nSubChallengesDue === 1) {
        dailyDigest.alert = `You have 1 challenge step today!`;
      } else {
        dailyDigest.alert = `You have ${nSubChallengesDue} challenge steps today!`;
      }
      let deviceToken = unsDoc.apn_token;
      const sendResult = await apnProvider.send(dailyDigest, deviceToken);

      console.log('dailyDigest: ', dailyDigest);
      console.log('deviceToken: ', deviceToken);
      console.log('sendResult: ', sendResult);

      nSuccessfulSends += sendResult['sent'].length;
      nFailedSends += sendResult['failed'].length;
    }

  }

  return {'nSuccessfulSends': nSuccessfulSends, 'nFailedSends': nFailedSends};

}

function getDigestRecipients(conn) {
  console.log('=> get digest recipients');

  const userNotificationSettings = conn.db("challe").collection("user_notification_settings");
  const maxCreatedAt = new Date();
  const hour = maxCreatedAt.getHours();

  const query = {
    "is_deleted": false,
    "created_at": {"$lte": maxCreatedAt},
    "receive_daily_challenge_digests": true,
    "receive_daily_challenge_digests_at_utc_hour": hour
  };
  console.log('=> query: ', query);

  const unsCursor = userNotificationSettings.find(query);

  return unsCursor;
}


function getNSubChallengesDue(conn, creatorId, creatorTZ) {
  console.log('=> get active challenges count');

  const userChallenges = conn.db("challe").collection("user_challenges");
  const now = new Date();

  // subchallenge dates are stored in UTC, but must be compared against users
  // local time to determine whether they are due today.
  const userTz = moment.tz.zone(creatorTZ);
  const userOffset = userTz.parse(now);
  const userNow = new Date(now.getTime() - userOffset * 60000);
  const userToday = new Date(userNow.getFullYear(), userNow.getMonth(), userNow.getDate());

  const query = {
    "is_deleted": false,
    "creator_id": creatorId
  };
  console.log('=> query: ', query);

  const pipeline = [
    {"$match": query},
    {"$project": {
      "_id": 0,
      "challenge_id": 1,
      "n_subchallenges": 1,
      "user_progression": 1,
      "is_complete": {"$eq": ["$n_subchallenges", "$n_completed_subchallenges"]}
      }
    },
    {"$match": {"is_complete": false}},
    {"$lookup": {
      "from": "challenges",
      "localField": "challenge_id",
      "foreignField": "_id",
      "as": "challenge"
      }
    }
  ];
  console.log('=> pipeline: ', pipeline);

  return userChallenges.aggregate(pipeline)
    .toArray()
    .then(incompleteUserChallenges => {
      console.log("=> incompleteUserChallenges: ", incompleteUserChallenges);

      let nSubChallengesDue = 0;

      for (var i = 0; i < incompleteUserChallenges.length; i++) {

        const userChallenge = incompleteUserChallenges[i];

        const challengeIsDeleted = userChallenge.challenge[0].is_deleted;

        if(challengeIsDeleted) {
          continue;
        }
        const userProgression = userChallenge.user_progression;
        const subChallenges = userChallenge.challenge[0].sub_challenges;

        for(var j = 0; j < userChallenge.n_subchallenges; j++) {

          const jStr = j.toString();
          const subChallengeIsComplete = userProgression[jStr].is_complete;
          const subChallengeDate = subChallenges[jStr].date;

          if(!subChallengeIsComplete && subChallengeDate.getTime() === userToday.getTime()) {
            nSubChallengesDue += 1;
          }

        }

      }

      return nSubChallengesDue;

    });

}
