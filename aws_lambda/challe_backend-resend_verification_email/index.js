"use strict";

var AWS = require('aws-sdk');
var secretsManagerClient = new AWS.SecretsManager({
    region: "us-east-1"
});
var secretName = "auth0/challe_backend/creds";
var secret;


module.exports.handler = (event, context, callback) => {
  context.callbackWaitsForEmptyEventLoop = false;

  console.log('event: ', event);

  const auth0UserId = event.headers.auth0_user_id;

  secretsManagerClient.getSecretValue({SecretId: secretName}, function(err, data) {
    if(err) {
      throw err;
    } else {
      secret = JSON.parse(data.SecretString);

      getAuth0AccessToken(secret.AUTH0_CLIENT_ID, secret.AUTH0_CLIENT_SECRET)
        .then(resp => {
          console.log('=> getAuth0AccessToken result: ', resp);

          const auth0AccessToken = resp.access_token;
          console.log(`auth0AccessToken: ${auth0AccessToken}`);

          requestAuth0EmailVerification(auth0AccessToken, auth0UserId)
            .then(resp => {
              console.log('=> requestAuth0EmailVerification result: ', resp);

              let body = resp;
              body["email_triggered"] = true;
              const toReturn = {"statusCode": 200, "body": JSON.stringify(body)};
              console.log("=> success! Returning: ", toReturn);
              callback(null, toReturn);
            })
            .catch(err => {
              console.log('=> an error occurred on update auth0 user: ', err);
              let body = err.error;
              body["email_triggered"] = false;
              const toReturn = {"statusCode": 200, "body": JSON.stringify(body)};
              console.log("=> failure, returning: ", toReturn);
              callback(null, toReturn);
            });

        })
        .catch(err => {
          console.log('=> an error occurred on get auth0 token: ', err);
          let body = err.error;
              body["email_triggered"] = false;
          callback(null, {"statusCode": 200, "body": JSON.stringify(body)});
        });
    }
  });

};


function getAuth0AccessToken(client_id, client_secret) {
  console.log('=> getAuth0AccessToken');

  var request = require('request-promise');

  var options = {
    method: 'POST',
    url: 'https://challe.auth0.com/oauth/token',
    headers: { 'content-type': 'application/json' },
    body:
    { grant_type: 'client_credentials',
      client_id: client_id,
      client_secret: client_secret,
      audience: 'https://challe.auth0.com/api/v2/' },
    json: true
  };

  console.log('Options: ', options);
  return request(options);
}

function requestAuth0EmailVerification(accessToken, userId) {
  console.log('=> requestAuth0EmailVerification');

  var request = require('request-promise');

  var options = {
    method: 'POST',
    url: 'https://challe.auth0.com/api/v2/jobs/verification-email',
    headers:
      { authorization: `Bearer ${accessToken}`,
        'content-type': 'application/json' },
    body: { "user_id": userId },
    json: true
  };

  console.log('Options: ', options);
  return request(options);
}
