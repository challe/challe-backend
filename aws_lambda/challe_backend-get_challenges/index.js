"use strict";

var AWS = require('aws-sdk');
var secretsManagerClient = new AWS.SecretsManager({
    region: "us-east-1"
});
var secretName = "mongodb/challe_backend/creds";
var secret;

const MongoClient = require('mongodb').MongoClient;

let cachedConn = null;


module.exports.handler = (event, context, callback) => {
  context.callbackWaitsForEmptyEventLoop = false;

  console.log('event: ', event);

  if(event.headers.is_keepalive_ping == "true") {
    console.log("ping!");
    callback(null, {"statusCode": 200, "body": "ping received!"});
  } else {

    secretsManagerClient.getSecretValue({SecretId: secretName}, function(err, data) {
      if(err) {
        throw err;
      } else {
        secret = JSON.parse(data.SecretString);

        connectToDatabase(secret.MONGODB_ATLAS_CLUSTER_URI)
        .then(conn => getChallenges(conn, event))
        .then(result => {
          console.log('=> returning result: ', result);
          callback(null, {"statusCode": 200, "body": JSON.stringify(result)});
        })
        .catch(err => {
          console.log('=> an error occurred: ', err);
          callback(err);
        });
      }
    });
  }

};


function connectToDatabase(uri) {
  console.log('=> connect to database');

  if (cachedConn) {
    console.log('=> using cached database instance');
    return Promise.resolve(cachedConn);
  }

  return MongoClient.connect(uri)
    .then(conn => {
      cachedConn = conn;
      return cachedConn;
    });
}

function getChallenges(conn, event) {

  // NOTE: simultaneous follower and query constraints not yet supported by Mongo
  // since can't compound indexes cannot contain both text and id.
  // Leaving this here in case that's someday not the case
  if(event.headers.follower) {
    const follower = event.headers.follower;

    var completed = null;
    if (event.headers.completed != null) {
      completed = (event.headers.completed === 'true');
    }

    return getFollowedChallengeIds(conn, follower, completed)
      .then(resp => {
        const followerChallengeIds = resp["followed"];
        return getChallenges(conn, event, followerChallengeIds);

      });

  } else {
    return getChallenges(conn, event);
  }

  function getChallenges(conn, event, challengeIds=null) {
    const currentUserId = event.headers.current_user_id;

    return getFollowedChallengeIds(conn, currentUserId)
      .then(resp => {
        const followedChallengeIds = resp["followed"];
        const completedChallengeIds = resp["completed"];

        return getFollowedUserIds(conn, currentUserId)
          .then(followedUserIds => {

            const nPerPage = parseInt(event.headers.n_per_page, 10);
            const pageNumber = parseInt(event.headers.page_number, 10);
            const maxCreatedAt = new Date(event.headers.max_created_at);
            const minStartDate = new Date(event.headers.min_start_date);
            const maxStartDate = new Date(event.headers.max_start_date);
            const creator = event.headers.creator;
            const sortBy = event.headers.sort_by;
            const sortOrder = event.headers.sort_order;
            const queryString = event.headers.query_string;

            const challenges = conn.db("challe").collection("challenges");

            let creatorFilt = {};
            if(creator == "any") {
              creatorFilt["$exists"] = true;
            } else if(creator == "following") {
              creatorFilt["$in"] = followedUserIds;
            } else {
              creatorFilt["$eq"] = creator;
            }

            let query = {
              "is_deleted": false,
              "n_flags": {"$lt": 3},
              "start_date": {
                "$gte": minStartDate,
                "$lte": maxStartDate
              },
              "created_at": {"$lte": maxCreatedAt},
              "$or": [
                {"visibility": "Everyone"},
                {"$and": [
                    {"visibility": "My followers"},
                    {"creator_id": {"$in": followedUserIds}}
                  ]
                },
                {"$and": [
                    {"visibility": "Link sharing only"},
                    {"$or": [
                      {"creator_id": currentUserId},
                      {"_id": {"$in": followedChallengeIds}},
                      {"_id": {"$in": completedChallengeIds}}
                    ]}
                  ]
                }
              ],
              "creator_id": creatorFilt
            };

            if(queryString) {
              query["$text"] = {"$search": queryString};
            }

            if(challengeIds) {
              query["_id"] = {"$in": challengeIds};
            } else {
              query["_id"] = {"$exists": true};
            }

            let sort = {};
            sort[sortBy] = parseInt(sortOrder, 10);

            const pipeline = [
              {"$match": query},
              {"$sort": sort},
              {"$skip": (pageNumber > 0 ? ( ( pageNumber - 1 ) * nPerPage ) : 0)},
              {"$limit": nPerPage}
            ];

            console.log(pipeline);

            return challenges.aggregate(pipeline)
              .toArray()
              .then(result => {
                const resLength = result.length;

                let followedChallengeIdStrs = [];
                if(followedChallengeIds) {
                  followedChallengeIdStrs = followedChallengeIds.map(String);
                }

                let completedChallengeIdStrs = [];
                if(completedChallengeIds) {
                  completedChallengeIdStrs = completedChallengeIds.map(String);
                }

                for (var i = 0; i < resLength; i++) {
                  const challengeId = String(result[i]._id); // indexOf doesn't work with mongo object arrays
                  const creatorId = result[i].creator_id;

                  if(followedChallengeIds) {
                    result[i].user_is_follower = (followedChallengeIdStrs.indexOf(challengeId) > -1);
                  } else {
                    result[i].user_is_follower = false;
                  }

                  if(completedChallengeIds) {
                    result[i].user_has_completed = (completedChallengeIdStrs.indexOf(challengeId) > -1);
                  } else {
                    result[i].user_has_completed = false;
                  }

                  if(followedUserIds) {
                    result[i].user_follows_creator = (followedUserIds.indexOf(creatorId) > -1);
                  } else {
                    result[i].user_follows_creator = false;
                  }
                }
                return result;

              })
              .catch(err => console.error(`Failed to find challenges: ${err}`));

          })
          .catch(err => console.error(`Failed to find followed user ids: ${err}`));

      })
      .catch(err => console.error(`Failed to find active challenge ids: ${err}`));

  }

}


function getFollowedChallengeIds(conn, creatorId, completed=null) {
  console.log('=> getFollowedChallengeIds');

  const userChallenges = conn.db("challe").collection('user_challenges');

  const query = {
    "is_deleted": false,
    "creator_id": creatorId
  };

  const pipeline = [
    {"$match": query},
    {"$project": {
      "_id": 0,
      "challenge_id": 1,
      "is_completed": {"$eq": ["$n_subchallenges", "$n_completed_subchallenges"]}
      }
    }
  ];

  if(completed === true) {
    pipeline.push({"$match": {"is_completed": true}});
  } else if(completed === false) {
    pipeline.push({"$match": {"is_completed": false}});
  }

  console.log('Pipeline: ', pipeline);

  return userChallenges.aggregate(pipeline)
    .toArray()
    .then(result => {
      console.log('Unprocessed result: ', result);

      let ids = [];
      let completed_ids = [];

      const resLength = result.length;
      for (var i = 0; i < resLength; i++) {
        ids.push(result[i].challenge_id);
        if(result[i].is_completed) {
          completed_ids.push(result[i].challenge_id);
        }
      }

      const toReturn = {"followed": ids, "completed": completed_ids};
      console.log('=> returning result: ', toReturn);
      return toReturn;
    })
    .catch(err => console.error(`Failed to find any user challenges: ${err}`));
}


function getFollowedUserIds(conn, creatorId, includeSelf=true, includePending=false, returnApproval=false) {
  console.log('=> getFollowedUserIds');

  const follows = conn.db("challe").collection('follows');

  let query = {
    "is_deleted": false,
    "is_approved": true,
    "creator_id": creatorId
  };

  if(includePending) {
   query["is_approved"] = {"$exists": true};
  }

  let pipeline = [
    {"$match": query},
    {"$project": {
      "_id": 0,
      "following_id": 1
      }
    }
  ];

  if(returnApproval) {
    pipeline[1]["$project"]["is_approved"] = 1;
  }

  console.log('Pipeline: ', pipeline);

  return follows.aggregate(pipeline)
    .toArray()
    .then(result => {
      console.log('Unprocessed result: ', result);

      const resLength = result.length;
      let ids = [];

      if(returnApproval) {

        for (var i = 0; i < resLength; i++) {
          ids.push(result[i]);
        }

        if(includeSelf){
          ids.push({"following_id": creatorId, "is_approved": true});
        }

      } else {

        for (var i = 0; i < resLength; i++) {
          ids.push(result[i].following_id);
        }

        if(includeSelf){
          ids.push(creatorId);
        }

      }

      console.log('=> returning result: ', ids);
      return ids;
    })
    .catch(err => console.error(`Failed to find any followed users: ${err}`));
}
