"use strict";

var AWS = require('aws-sdk');
var secretsManagerClient = new AWS.SecretsManager({
    region: "us-east-1"
});
var secretName = "mongodb/challe_backend/creds";
var secret;

const MongoClient = require('mongodb').MongoClient;

let cachedConn = null;


module.exports.handler = (event, context, callback) => {
  context.callbackWaitsForEmptyEventLoop = false;

  console.log('event: ', event);

  if(event.headers.is_keepalive_ping == "true") {
    console.log("ping!");
    callback(null, {"statusCode": 200, "body": "ping received!"});
  } else {
    secretsManagerClient.getSecretValue({SecretId: secretName}, function(err, data) {
      if(err) {
        throw err;
      } else {
        secret = JSON.parse(data.SecretString);

        connectToDatabase(secret.MONGODB_ATLAS_CLUSTER_URI)
          .then(conn => updateMongoUser(conn, event))
          .then(result => {
            console.log('=> returning result: ', result);
            callback(null, {"statusCode": 200, "body": JSON.stringify(result)});
          })
          .catch(err => {
            console.log('=> an error occurred upon updating user: ', err);
            const body = {"update_complete": false};
            callback(null, {"statusCode": 200, "body": JSON.stringify(body)});
          });
      }
    });
  }
};


function connectToDatabase (uri) {
    console.log('=> connect to database');

    if (cachedConn) {
        console.log('=> using cached database instance');
        return Promise.resolve(cachedConn);
    }

    return MongoClient.connect(uri)
      .then(conn => {
        cachedConn = conn;
        return cachedConn;
      });
}


function updateMongoUser(conn, event) {
  console.log('=> updating mongo user');

  const users = conn.db("challe").collection("users");

  const userId = event.headers.user_id;

  const now = new Date();
  let fieldsUpdate = {updated_at: now};
  let challengesFieldsUpdate = {};
  let commentsFieldsUpdate = {};

  if("email" in event.headers) {
    fieldsUpdate.email = event.headers.email.toLowerCase();
  }

  if("username" in event.headers) {
    fieldsUpdate.username = event.headers.username.toLowerCase();
    challengesFieldsUpdate.creator = event.headers.username.toLowerCase();
    commentsFieldsUpdate.creator = event.headers.username.toLowerCase();
  }

  if("first_name" in event.headers) {
    fieldsUpdate.first_name = event.headers.first_name;
  }

  if("last_name" in event.headers) {
    fieldsUpdate.last_name = event.headers.last_name;
  }

  if("phone_number" in event.headers) {
    fieldsUpdate.phone_number = event.headers.phone_number;
  }

  if("bio" in event.headers) {
    fieldsUpdate.bio = event.headers.bio;
  }

  if("is_private" in event.headers) {
    fieldsUpdate.is_private = (event.headers.is_private == 'Yes');
  }

  if("timezone" in event.headers) {
    fieldsUpdate.timezone = event.headers.timezone;
  }

  if("image_url" in event.headers) {
    fieldsUpdate.image_url = event.headers.image_url;
    challengesFieldsUpdate.creator_image_url = event.headers.image_url;
    commentsFieldsUpdate.creator_image_url = event.headers.image_url;
  }

  const update = {"$set": fieldsUpdate};
  console.log('=> update: ', update);

  const query = {"creator_id": userId};
  console.log('=> query: ', query);

  const options = {"upsert": false};
  console.log('=> options: ', options);

  return users.updateOne(query, update, options)
    .then(result => {
      console.log('User update complete, result: ', result);
      const { matchedCount, modifiedCount } = result;

      if(matchedCount && modifiedCount) {
        console.log(`Successfully updated user.`);

        if(Object.entries(challengesFieldsUpdate).length === 0 && challengesFieldsUpdate.constructor === Object) {
          console.log('No challenges or comments updates necessary...');
          const body = {"update_complete": true};
          return body;

        } else {
            console.log('Challenges and comments updates necessary...');

            const challengesUpdate = {"$set": challengesFieldsUpdate};
            console.log('Challenges update: ', challengesUpdate);
            return updateChallenges(conn, userId, challengesUpdate)
            .then(result => {
              console.log('Challenges update result: ', result);

                const commentsUpdate = {"$set": commentsFieldsUpdate};
                console.log('Comments update: ', commentsUpdate);
                return updateComments(conn, userId, commentsUpdate)
                .then(result => {
                  console.log('Comments update result: ', result);
                  const body = {"update_complete": true};
                  return body;
                })
                .catch(err => {
                  console.log('=> an error occurred upon updating comments: ', err);
                  const body = {"update_complete": false};
                  return body;
                });
            })
          .catch(err => {
            console.log('=> an error occurred upon updating challenges: ', err);
            const body = {"update_complete": false};
            return body;
          });
        }

      } else {
        console.log(`Failed to update user.`);
        const body = {"update_complete": false};
        return body;
      }

    })
    .catch(err => {
      console.error(`Failed to update user: ${err}`);
      return err;
      });
}


function updateChallenges(conn, userId, update) {
  console.log('=> updating challenges');

  const challenges = conn.db("challe").collection("challenges");

  const query = {"creator_id": userId};
  console.log('=> query: ', query);

  console.log('=> update: ', update);

  const options = {"upsert": false};
  console.log('=> options: ', options);

  return challenges.updateMany(query, update, options)
    .then(result => {
      console.log('Challenges update complete, result: ', result);
      const { matchedCount, modifiedCount } = result;
      if(matchedCount && modifiedCount) {
        console.log(`Successfully updated challenges.`);
      }
      return result;
    })
    .catch(err => {
      console.error(`Failed to update challenges: ${err}`);
      return err;
      });
}


function updateComments(conn, userId, update) {
  console.log('=> update comments');

  const comments = conn.db("challe").collection("comments");

  const query = {"creator_id": userId};
  console.log('=> query: ', query);

  console.log('=> update: ', update);

  const options = {"upsert": false};
  console.log('=> options: ', options);

  return comments.updateMany(query, update, options)
    .then(result => {
      console.log('Comments update complete, result: ', result);
      const { matchedCount, modifiedCount } = result;
      if(matchedCount && modifiedCount) {
        console.log(`Successfully updated comments.`);
      }
      return result;
    })
    .catch(err => {
      console.error(`Failed to update comments: ${err}`);
      return err;
      });
}
