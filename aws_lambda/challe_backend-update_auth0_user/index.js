"use strict";

var AWS = require('aws-sdk');
var secretsManagerClient = new AWS.SecretsManager({
    region: "us-east-1"
});
var secretName = "auth0/challe_backend/creds";
var secret;


module.exports.handler = (event, context, callback) => {
  context.callbackWaitsForEmptyEventLoop = false;

  console.log('event: ', event);

  const auth0UserId = event.headers.auth0_user_id;

  console.log('=> updateAuth0User');

  let update = {};
  let userMetadata = {};

  var emailChanged = false;
  if("email" in event.headers) {
    update.email = event.headers.email.toLowerCase();
    emailChanged = true;
  }

  if("username" in event.headers) {
    update.username = event.headers.username.toLowerCase();
  }

  if("first_name" in event.headers) {
    userMetadata.first_name = event.headers.first_name;
  }

  if("last_name" in event.headers) {
    userMetadata.last_name = event.headers.last_name;
  }

  if("phone_number" in event.headers) {
    userMetadata.phone_number = event.headers.phone_number;
  }

  if("image_url" in event.headers) {
    userMetadata.picture = event.headers.image_url;
  }

  if("timezone" in event.headers) {
    userMetadata.timezone = event.headers.timezone;
  }

  if(!(Object.entries(userMetadata).length === 0 && userMetadata.constructor === Object)) {
    update.user_metadata = userMetadata;
  }

  console.log('userMetadata: ', userMetadata);
  console.log('update: ', update);

  // handle empty case
  if(Object.entries(update).length === 0 && update.constructor === Object) {
    const body = {"update_complete": true};
    const toReturn = {"statusCode": 200, "body": JSON.stringify(body)};
    console.log('Nothing to update! Returning: ', toReturn);
    callback(null, toReturn);
  } else {

    secretsManagerClient.getSecretValue({SecretId: secretName}, function(err, data) {
      if(err) {
        throw err;
      } else {
        secret = JSON.parse(data.SecretString);

        getAuth0AccessToken(secret.AUTH0_CLIENT_ID, secret.AUTH0_CLIENT_SECRET)
          .then(resp => {
            const auth0AccessToken = resp.access_token;

            updateAuth0User(auth0AccessToken, auth0UserId, update)
              .then(resp => {
                console.log(resp);

                if(emailChanged) {

                  requestAuth0EmailVerification(auth0AccessToken, auth0UserId)
                    .then(resp => {
                      let body = resp;
                      body["update_complete"] = true;
                      const toReturn = {"statusCode": 200, "body": JSON.stringify(body)};
                      console.log("=> success! Returning: ", toReturn);
                      callback(null, toReturn);
                    })
                    .catch(err => {
                      console.log('=> an error occurred on update auth0 user: ', err);
                      let body = err.error;
                      body["update_complete"] = false;
                      const toReturn = {"statusCode": 200, "body": JSON.stringify(body)};
                      console.log("=> failure, returning: ", toReturn);
                      callback(null, toReturn);
                    });

                } else {
                  let body = resp;
                  body["update_complete"] = true;
                  const toReturn = {"statusCode": 200, "body": JSON.stringify(body)};
                  console.log("=> success! Returning: ", toReturn);
                  callback(null, toReturn);
                }
              })
              .catch(err => {
                console.log('=> an error occurred on update auth0 user: ', err);
                let body = err.error;
                body["update_complete"] = false;
                const toReturn = {"statusCode": 200, "body": JSON.stringify(body)};
                console.log("=> failure, returning: ", toReturn);
                callback(null, toReturn);
              });

          })
          .catch(err => {
            console.log('=> an error occurred on get auth0 token: ', err);
            let body = err.error;
            body["update_complete"] = false;
            const toReturn = {"statusCode": 200, "body": JSON.stringify(body)};
            console.log("=> failure, returning: ", toReturn);
            callback(null, toReturn);
          });
      }
    });
  }
};


function getAuth0AccessToken(client_id, client_secret) {
  console.log('=>getAuth0AccessToken');

  var request = require('request-promise');

  var options = {
    method: 'POST',
    url: 'https://challe.auth0.com/oauth/token',
    headers: { 'content-type': 'application/json' },
    body:
    { grant_type: 'client_credentials',
      client_id: client_id,
      client_secret: client_secret,
      audience: 'https://challe.auth0.com/api/v2/' },
    json: true
  };
  console.log('Options: ', options);

  return request(options);
}


function updateAuth0User(accessToken, userId, update) {
  console.log('=>updateAuth0User');

  var request = require('request-promise');

  var options = {
    method: 'PATCH',
    url: `https://challe.auth0.com/api/v2/users/${userId}`,
    headers:
      { authorization: `Bearer ${accessToken}`,
        'content-type': 'application/json' },
    body: update,
    json: true
  };
  console.log('Options: ', options);

  return request(options);
}

function requestAuth0EmailVerification(accessToken, userId) {
  console.log('=>requestAuth0EmailVerification');

  var request = require('request-promise');

  var options = {
    method: 'POST',
    url: 'https://challe.auth0.com/api/v2/jobs/verification-email',
    headers:
      { authorization: `Bearer ${accessToken}`,
        'content-type': 'application/json' },
    body: { "user_id": userId },
    json: true
  };
  console.log('Options: ', options);

  return request(options);
}
