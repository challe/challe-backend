"use strict";

var AWS = require('aws-sdk');
var secretsManagerClient = new AWS.SecretsManager({
    region: "us-east-1"
});
var apnSecretName = "apn/challe_backend/creds";
var apnSecret;
var mongoSecretName = "mongodb/challe_backend/creds";
var mongoSecret;

const apn = require('apn');
const moment = require('moment-timezone');
const MongoClient = require('mongodb').MongoClient;

let cachedConn = null;


module.exports.handler = (event, context, callback) => {
  context.callbackWaitsForEmptyEventLoop = false;

  console.log('event: ', event);

  secretsManagerClient.getSecretValue({SecretId: mongoSecretName}, function(err, data) {
    if(err) {
      throw err;
    } else {
      mongoSecret = JSON.parse(data.SecretString);

      secretsManagerClient.getSecretValue({SecretId: apnSecretName}, function(err, data) {
        if(err) {
          throw err;
        } else {
          apnSecret = JSON.parse(data.SecretString);

          connectToMongoDatabase(mongoSecret.MONGODB_ATLAS_CLUSTER_URI)
            .then(conn => sendChallengeReminders(
              conn, event.production, apnSecret.APN_KEY, apnSecret.APN_KEY_ID,
              apnSecret.APN_TEAM_ID, apnSecret.APN_BUNDLE_ID
            ))
            .then(result => {
              console.log('=> returning result: ', result);
              callback(null, {"statusCode": 200, "body": JSON.stringify(result)});
            })
            .catch(err => {
              console.log('=> an error occurred upon sending challenge reminders: ', err);
              const body = {"update_complete": false};
              callback(null, {"statusCode": 200, "body": JSON.stringify(body)});
            });
        }
      });
    }
  });
};


function connectToMongoDatabase (uri) {
  console.log('=> connect to database');

  if (cachedConn) {
      console.log('=> using cached database instance');
      return Promise.resolve(cachedConn);
  }

  return MongoClient.connect(uri)
    .then(conn => {
      cachedConn = conn;
      return cachedConn;
    });
}


async function sendChallengeReminders(conn, production, key, keyId, teamId, bundleId) {

  let ucsCursor = await getUserChallengeSteps(conn);

  const apnOptions = {
    token: {
      key: '\n-----BEGIN PRIVATE KEY-----\n'+key+'\n-----END PRIVATE KEY-----',
      keyId: keyId,
      teamId: teamId
    },
    production: production,
    connectionRetryLimit: 1
  };

  const apnProvider = new apn.Provider(apnOptions);

  let nSuccessfulSends = 0;
  let nFailedSends = 0;

  while(await ucsCursor.hasNext()) {

    const ucsDoc = await ucsCursor.next();
    console.log('=> ucsDoc: ', ucsDoc);

    const nSubChallengesDue = await getNSubChallengesDue(ucsDoc);
    console.log('=> nChallengeStepsDue: ', nSubChallengesDue);

    if(nSubChallengesDue > 0) {

      let challengeReminder = new apn.Notification();
      challengeReminder.badge = 0;
      challengeReminder.sound = "ping.aiff";
      challengeReminder.topic = bundleId;

      challengeReminder.payload = {
        'initial_view_identifier': 'challengeViewController',
        'challenge_id': ucsDoc._id
      };

      const challengeTitle = decodeURIComponent(JSON.parse('"'+ucsDoc.title+'"'));

      if (nSubChallengesDue === 1) {
        challengeReminder.alert = `Your ongoing challenge '${challengeTitle}' has a step due today 😊`;
      } else {
        challengeReminder.alert = `Your ongoing challenge '${challengeTitle}' has ${nSubChallengesDue} steps due today 😊`;
      }
      let deviceToken = ucsDoc.user_notification_settings.apn_token;
      const sendResult = await apnProvider.send(challengeReminder, deviceToken);

      console.log('challengeReminder: ', challengeReminder);
      console.log('deviceToken: ', deviceToken);
      console.log('sendResult: ', sendResult);

      nSuccessfulSends += sendResult['sent'].length;
      nFailedSends += sendResult['failed'].length;
    }

  }

  return {'nSuccessfulSends': nSuccessfulSends, 'nFailedSends': nFailedSends};

}


function getUserChallengeSteps(conn) {
  console.log('=> get user challenge steps');

  const challenges = conn.db("challe").collection("challenges");
  let maxCreatedAt = new Date();
  const hour = maxCreatedAt.getHours();

  const pipeline = [
    {'$match': {
        'is_deleted': false,
        'start_date': {'$lte': new Date(maxCreatedAt.setDate(maxCreatedAt.getDate() + 3))},
        'end_date': {'$gte': new Date(maxCreatedAt.setDate(maxCreatedAt.getDate() - 3))},
        'created_at': {'$lte': maxCreatedAt}
      }
    },
    {'$project': {
        'title': 1,
        'sub_challenges': 1
      }
    },
    {'$lookup': {
        'from': 'user_challenges',
        'localField': '_id',
        'foreignField': 'challenge_id',
        'as': 'user_challenges'
      }
    },
    {'$unwind': {
        'path': '$user_challenges'
      }
    },
    {'$match': {
        'user_challenges.is_deleted': false,
        'user_challenges.created_at': {'$lte': maxCreatedAt},
        'user_challenges.receive_challenge_reminders': true,
        'user_challenges.receive_challenge_reminders_at_utc_hour': hour
      }
    },
    {'$lookup': {
        'from': 'user_notification_settings',
        'localField': 'user_challenges.creator_id',
        'foreignField': 'creator_id',
        'as': 'user_notification_settings'
      }
    },
    {'$unwind': {
        'path': '$user_notification_settings'
      }
    },
    {'$match': {
        'user_notification_settings.is_deleted': false
      }
    },
    {'$sort': {
        'user_challenges.creator_id': 1
      }
    }
  ];
  console.log('=> pipeline: ', pipeline);

  const ucsCursor = challenges.aggregate(pipeline);

  return ucsCursor;

}


function getNSubChallengesDue(ucsDoc) {
  console.log('=> get active subchallenges count');

  const userChallenge = ucsDoc.user_challenges;
  const userProgression = userChallenge.user_progression;

  const subChallenges = ucsDoc.sub_challenges;

  let now = new Date();

  // subchallenge dates are stored in UTC, but must be compared against users'
  // local time to determine whether they are due today.
  const userTz = moment.tz.zone(ucsDoc.user_notification_settings.timezone);
  const userOffset = userTz.parse(now);
  const userNow = new Date(now.getTime() - userOffset * 60000);
  const userToday = new Date(userNow.getFullYear(), userNow.getMonth(), userNow.getDate());

  let nSubChallengesDue = 0;

  for(var j = 0; j < userChallenge.n_subchallenges; j++) {

    const jStr = j.toString();
    const subChallengeIsComplete = userProgression[jStr].is_complete;
    const subChallengeDate = subChallenges[jStr].date;

    if(!subChallengeIsComplete && subChallengeDate.getTime() === userToday.getTime()) {
      nSubChallengesDue += 1;
    }

  }

  return nSubChallengesDue;

}
