"use strict";

module.exports.handler = (event, context, callback) => {
  context.callbackWaitsForEmptyEventLoop = false;

  console.log('event: ', event);

  console.log('=> getProfilePhotoUploadURL');

  var AWS = require('aws-sdk')
  var uuid = require('uuid')

  AWS.config.update({
    region: 'us-east-1'
  });

  var S3 = new AWS.S3();
  const bucket = 'challe-userdata';
  const userId = event.headers.user_id;

  var key = uuid.v4();

  // Record metadata in the DB, associate it with the user
  // Construct a path where data will be stored in that bucket
  var path = 'profile-photos/' + userId + '/' + key + '.jpg';

  // Construct a request
  var request = {
    Bucket: bucket,
    Key: path,
    Expires: 300, // Valid for only 5 minutes
    ACL: 'public-read'
  };

  console.log('Request: ', request);

  // Ask
  S3.getSignedUrl('putObject', request, function(err, result) {

    console.log('Result: ', result);

    const body = {
      "upload_url": result,
      "photo_url": result.substr(0, result.indexOf('?'))
    }

    const resp = {"statusCode": 200, "body": JSON.stringify(body)};
    console.log('Response: ', resp);
    callback(null, resp);

  });

};
