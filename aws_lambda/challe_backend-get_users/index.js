"use strict";

var AWS = require('aws-sdk');
var secretsManagerClient = new AWS.SecretsManager({
    region: "us-east-1"
});
var secretName = "mongodb/challe_backend/creds";
var secret;

const MongoClient = require('mongodb').MongoClient;
const MONGODB_URI = process.env.MONGODB_ATLAS_CLUSTER_URI; // or Atlas connection string

let cachedConn = null;

module.exports.handler = (event, context, callback) => {
  context.callbackWaitsForEmptyEventLoop = false;

  console.log('event: ', event);

  if(event.headers.is_keepalive_ping == "true") {
    console.log("ping!");
    callback(null, {"statusCode": 200, "body": "ping received!"});
  } else {

    secretsManagerClient.getSecretValue({SecretId: secretName}, function(err, data) {
      if(err) {
        throw err;
      } else {
        secret = JSON.parse(data.SecretString);

        connectToDatabase(secret.MONGODB_ATLAS_CLUSTER_URI)
        .then(conn => getUsers(conn, event))
        .then(result => {
          console.log('=> returning result: ', result);
          callback(null, {"statusCode": 200, "body": JSON.stringify(result)});
        })
        .catch(err => {
          console.log('=> an error occurred: ', err);
          callback(err);
        });
      }
    });
  }
};


function connectToDatabase (uri) {
  console.log('=> connect to database');

  if (cachedConn) {
    console.log('=> using cached database instance');
    return Promise.resolve(cachedConn);
  }

  return MongoClient.connect(uri)
    .then(conn => {
      cachedConn = conn;
      return cachedConn;
    });
}

function getUsers(conn, event, withholdPII=true) {
    console.log('=> getting users');

    const currentUserId = event.headers.current_user_id;

    return getFollowedUserIds(conn, currentUserId, true, true, true)
      .then(followedUserIds => {

        const nPerPage = parseInt(event.headers.n_per_page, 10);
        const pageNumber = parseInt(event.headers.page_number, 10);
        const maxCreatedAt = new Date(event.headers.max_created_at);
        const queryString = event.headers.query_string;

        const users = conn.db("challe").collection("users");

        let query = {
          "is_deleted": false,
          "n_flags": {"$lt": 3},
          "created_at": {"$lte": maxCreatedAt},
        };

        if(queryString) {
          query["$text"] = {"$search": queryString};
        }

        let pipeline = [
          {"$match": query},
          {"$skip": (pageNumber > 0 ? ( ( pageNumber - 1 ) * nPerPage ) : 0)},
          {"$limit": nPerPage}
        ];

        if(withholdPII) {
          const project = {
            "$project": {
             "phone_number": 0,
             "email": 0
            }
          };
          pipeline.push(project);
        }

        console.log('=> pipeline: ', pipeline);

        return users.aggregate(pipeline)
          .toArray()
          .then(result => {

            console.log('=> unprocessed result: ', result);

            let usersData = [];
            const resLength = result.length;

            for (var i = 0; i < resLength; i++) {
              const userData = result[i];
              const userId = userData.creator_id; // indexOf doesn't work with mongo object arrays
              userData.user_is_follower = false; // initialize
              userData.follow_request_pending = false; // initialize

              if(followedUserIds) {

               const fULength = followedUserIds.length;
               for (var j = 0; j < fULength; j++) {
                  const followedUserId = followedUserIds[j]["following_id"];
                  if(userId == followedUserId) {
                    userData.follow_request_pending = !followedUserIds[j]["is_approved"];
                    userData.user_is_follower = !userData.follow_request_pending;
                    break;
                  }
               }

              }

              usersData.push(userData);
            }

            console.log('=> returning result: ', usersData);
            return usersData;

          })
          .catch(err => console.error(`Failed to find challenges: ${err}`));

      })
      .catch(err => console.error(`Failed to find followed user ids: ${err}`));

}


function getFollowedUserIds(conn, creatorId, includeSelf=true, includePending=false, returnApproval=false) {
  console.log('=> getting followed user ids');

  const follows = conn.db("challe").collection('follows');

  let query = {
    "is_deleted": false,
    "is_approved": true,
    "creator_id": creatorId
  };

  if(includePending) {
   query["is_approved"] = {"$exists": true};
  }

  let pipeline = [
    {"$match": query},
    {"$project": {
      "_id": 0,
      "following_id": 1
      }
    }
  ];

  if(returnApproval) {
    pipeline[1]["$project"]["is_approved"] = 1;
  }

  console.log('=> pipeline: ', pipeline);

  return follows.aggregate(pipeline)
    .toArray()
    .then(result => {
      console.log('=> unprocessed result: ', result);

      const resLength = result.length;
      let ids = [];

      if(returnApproval) {

        for (var i = 0; i < resLength; i++) {
          ids.push(result[i]);
        }

        if(includeSelf){
          ids.push({"following_id": creatorId, "is_approved": true});
        }

      } else {

        for (var i = 0; i < resLength; i++) {
          ids.push(result[i].following_id);
        }

        if(includeSelf){
          ids.push(creatorId);
        }

      }

      console.log('=> returning result: ', ids);
      return ids;
    })
    .catch(err => console.error(`Failed to find any followed users: ${err}`));
}
