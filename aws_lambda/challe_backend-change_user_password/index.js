"use strict";

module.exports.handler = (event, context, callback) => {
  context.callbackWaitsForEmptyEventLoop = false;

  console.log('event: ', event);

  const userEmail = event.headers.user_email;

  requestAuth0ChangePassword(userEmail)
    .then(resp => {
      console.log('=> requestAuth0ChangePassword result: ', resp);
      let body = {"email_triggered": true};
      const toReturn = {"statusCode": 200, "body": JSON.stringify(body)};
      console.log("=> success! Returning: ", toReturn);
      callback(null, toReturn);
    })
    .catch(err => {
      console.log('=> an error occurred on update auth0 user: ', err);
      let body = err.error;
      body["email_triggered"] = false;
      const toReturn = {"statusCode": 200, "body": JSON.stringify(body)};
      console.log("=> failure, returning: ", toReturn);
      callback(null, toReturn);
    });

};


function requestAuth0ChangePassword(userEmail) {
  console.log('=> requestAuth0ChangePassword');

  var request = require('request-promise');

  var options = {
    method: 'POST',
    url: 'https://challe.auth0.com/dbconnections/change_password',
    headers: { 'content-type': 'application/json' },
    body:
    { email: userEmail,
      connection: 'Username-Password-Authentication' },
    json: true
  };
  console.log('Options: ', options);

  return request(options);
}
